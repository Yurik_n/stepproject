console.log("carusel working")
const forward = document.querySelector(".forward")
const back = document.querySelector(".back")

const personName =document.querySelectorAll(".person")
const mainPhoto = document.querySelectorAll(".main-photo-person")
const caruselPhoto = document.querySelectorAll(".photo-carusel")

let indexOfImages = 0
let indexOfPersonName = 0
let indexOfCarusel = 1


const changeImages = (n) => {
    mainPhoto[indexOfImages].classList.add("hidden")
    indexOfImages = (n+mainPhoto.length) % mainPhoto.length
    mainPhoto[indexOfImages].classList.remove("hidden")
}
const changeName = (n) => {
    personName[indexOfPersonName].classList.add("hidden")
    indexOfPersonName = (n+personName.length) % personName.length
    personName[indexOfPersonName ].classList.remove("hidden")
}
const changeCaruselForward = () => {
    caruselPhoto.forEach(element => {
        element.classList.remove("top")
    }); 
    caruselPhoto[indexOfCarusel].classList.add("top")
    indexOfCarusel = (indexOfCarusel+1) % caruselPhoto.length
    caruselPhoto[indexOfCarusel].classList.remove("top")
}
const changeCaruselBack = (n) => {
    caruselPhoto.forEach(element => {
        element.classList.remove("top")
    });   
    caruselPhoto[indexOfCarusel].classList.add("top")     
    indexOfCarusel = (n+caruselPhoto.length) % caruselPhoto.length   
}

forward.addEventListener("click", () => {
    changeImages(indexOfImages+1)
    changeName(indexOfPersonName+1)   
    changeCaruselForward()
})
back.addEventListener("click", () => {
    changeImages(indexOfImages-1)
    changeName(indexOfPersonName-1)
   changeCaruselBack(indexOfCarusel-1)
})