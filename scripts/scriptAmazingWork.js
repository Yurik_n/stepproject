console.log("amazingIsWorking")

const amazingButton = document.querySelector(".amazing-button")
const amazingGrid = Array.from(document.querySelectorAll(".amazing-image"))
const allTab = document.querySelector(".amazing-menu-item")
const loadImage = Array.from(document.querySelectorAll(".load"))

amazingButton.addEventListener("click", (e) => {
    amazingButton.classList.add("hidden")
    loadImage.forEach((element, i) => {
        element.classList.remove("hidden")
    });
})

const filterAmazingWork = document.querySelectorAll(".item-filter")

filterAmazingWork.forEach(el => {
    el.addEventListener("click", (e) => {
        let filter = e.target.dataset["filter"]
        amazingGrid.forEach(el => {
            el.classList.remove("hidden")
            if (!el.classList.contains(filter)) {
                el.classList.add("hidden")
            }
        })
    })
})

allTab.addEventListener("click", () => {
    amazingGrid.forEach(element => {
        element.classList.remove("hidden")
    })
})